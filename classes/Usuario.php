<?php

//Questão 3
class Usuario
{

  public $nome = '';
  public $email = '';

  public function getNome() : string
  {
    return $this->nome;
  }

  public function setNome(string $nome)
  {
    if(!empty($nome)){
      $this->nome = $nome;
    }
  }

  public function getEmail() : string
  {
    return $this->email;
  }

  public function setEmail(string $email)
  {
    if(filter_var($email,FILTER_VALIDATE_EMAIL)){
      $this->email = $email;
    }
  }

}