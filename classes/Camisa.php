<?php

//Questão 1
class Camisa
{
  private $marca = '';
  private $modelo = '';
  private $tamanho = '';
  private $cor = null;

  public function getMarca() : string
  {
    return $this->marca;
  }

  public function setMarca(string $marca)
  {
    if(!empty($marca)){
      $this->marca = $marca;
    }
  }

  public function getModelo() : string
  {
    return $this->modelo;
  }
  
  public function setModelo(string $modelo)
  {
    if(!empty($modelo)){
      $this->modelo = $modelo;
    }
  }

  public function getTamanho() : string
  {
    return $this->tamanho;
  }
  
  public function setTamanho(string $tamanho)
  {
    if( !empty($tamanho) ){
      $this->tamanho = $tamanho;
    }
  }

  public function getCor() : Cor
  {
    if(is_null($this->cor)){
      return new Cor();
    }
    return $this->cor;
  }

  public function setCor(Cor $cor)
  {
    $this->cor = $cor;
  }

  public function produzir() : bool
  { 
    /*
      Produz Aqui e Retorna true ou false respectivamente
      em caso de sucesso ou falha.
    */
    return true;
  }

}
