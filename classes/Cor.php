<?php

//Questão 1
class Cor
{
  public $vermelho = 1;
  public $verde = 2;
  public $azul = 3;

  public function getVermelho() : int
  {
    return $this->vermelho;
  }

  public function setVermelho(int $vermelho)
  {
    $this->vermelho = $vermelho;
  }

  public function getVerde() : int
  {
    return $this->verde;
  }

  public function setVerde(int $verde)
  {
    $this->verde = $verde;
  }

  public function getAzul() : int
  {
    return $this->azul;
  }

  public function setAzul(int $azul)
  {
    $this->azul = $azul;
  }

}