<?php

//Questão 3
class Video
{
  private $titulo = '';
  private $url = '';
  private $visibilidade = 'oculto';
  private $usuario = null;

  public function getTitulo() : string
  {
    return $this->titulo;
  }

  public function setTitulo(string $titulo)
  {
    if(!empty($titulo)){
      $this->titulo = $titulo;
    }
  }

  public function getUrl() : string
  {
    return $this->url;
  }

  public function setUrl(string $url)
  {
    if(filter_var($url,FILTER_VALIDATE_URL) ){
      $this->url = $url;
    }
  }

  public function getVisibilidade() : string
  {
    return $this->visibilidade;
  }

  public function setVisibilidade(string $visibilidade)
  {
    if((strtolower($visibilidade) === 'oculto') | (strtolower($visibilidade) === 'listado')){
      $this->visibilidade = strtolower($visibilidade);
    }
  }

  public function getUsuario() : Usuario
  {
    if(is_null($this->usuario)){
      return new Usuario();
    }
    return $this->usuario;
  }

  public function setUsuario(Usuario $usuario)
  {
    $this->usuario = $usuario;
  }

  public function ocultarVideo(): bool
  {
    if($this->visibilidade === 'listado'){
      $this->setVisibilidade('oculto');
      return true;
    }
    return false;
  }

  public function listarVideo(): bool
  {
    if($this->visibilidade === 'oculto'){
      $this->setVisibilidade('listado');
      return true;
    }
    return false;
  }

}