# Classes 


## Questões 1 e 2

![](img/1_2.png)

### 1
```php

class Camisa
{
  private $marca = '';
  private $modelo = '';
  private $tamanho = '';
  private $cor = null;

  public function getMarca() : string
  {
    return $this->marca;
  }

  public function setMarca(string $marca)
  {
    if(!empty($marca)){
      $this->marca = $marca;
    }
  }

  public function getModelo() : string
  {
    return $this->modelo;
  }
  
  public function setModelo(string $modelo)
  {
    if(!empty($modelo)){
      $this->modelo = $modelo;
    }
  }

  public function getTamanho() : string
  {
    return $this->tamanho;
  }
  
  public function setTamanho(string $tamanho)
  {
    if( !empty($tamanho) ){
      $this->tamanho = $tamanho;
    }
  }

  public function getCor() : Cor
  {
    if( is_null($this->cor) ){
      return new Cor();
    }
    return $this->cor;
  }

  public function setCor(Cor $cor)
  {
    $this->cor = $cor;
  }

  public function produzir() : bool
  { 
    /*
      Produz Aqui e Retorna true ou false respectivamente
      em caso de sucesso ou falha.
    */
    return true;
  }

}


class Cor
{
  public $vermelho = 1;
  public $verde = 2;
  public $azul = 3;

  public function getVermelho() : int
  {
    return $this->vermelho;
  }

  public function setVermelho(int $vermelho)
  {
    $this->vermelho = $vermelho;
  }

  public function getVerde() : int
  {
    return $this->verde;
  }

  public function setVerde(int $verde)
  {
    $this->verde = $verde;
  }

  public function getAzul() : int
  {
    return $this->azul;
  }

  public function setAzul(int $azul)
  {
    $this->azul = $azul;
  }

}

```


### 2
> O relacionamento apresentado é uma Agregação, quando uma classe precisa da outra para executar sua ação, ou seja, uma classe utiliza a outra como parte de si própria.


## Questão 3
![](img/3.png)

```php

class Video
{
  private $titulo = '';
  private $url = '';
  private $visibilidade = 'oculto';
  private $usuario = null;

  public function getTitulo() : string
  {
    return $this->titulo;
  }

  public function setTitulo(string $titulo)
  {
    if(!empty($titulo)){
      $this->titulo = $titulo;
    }
  }

  public function getUrl() : string
  {
    return $this->url;
  }

  public function setUrl(string $url)
  {
    if(filter_var($url,FILTER_VALIDATE_URL) ){
      $this->url = $url;
    }
  }

  public function getVisibilidade() : string
  {
    return $this->visibilidade;
  }

  public function setVisibilidade(string $visibilidade)
  {
    if((strtolower($visibilidade) === 'oculto') | (strtolower($visibilidade) === 'listado')){
      $this->visibilidade = strtolower($visibilidade);
    }
  }

  public function getUsuario() : Usuario
  {
    if( is_null($this->usuario) ){
      return new Usuario();
    }
    return $this->usuario;
  }

  public function setUsuario(Usuario $usuario)
  {
    $this->usuario = $usuario;
  }

  public function ocultarVideo(): bool
  {
    if($this->visibilidade === 'listado'){
      $this->setVisibilidade('oculto');
      return true;
    }
    return false;
  }

  public function listarVideo(): bool
  {
    if($this->visibilidade === 'oculto'){
      $this->setVisibilidade('listado');
      return true;
    }
    return false;
  }

}



class Usuario
{

  public $nome = '';
  public $email = '';

  public function getNome() : string
  {
    return $this->nome;
  }

  public function setNome(string $nome)
  {
    if(!empty($nome)){
      $this->nome = $nome;
    }
  }

  public function getEmail() : string
  {
    return $this->email;
  }

  public function setEmail(string $email)
  {
    if(filter_var($email,FILTER_VALIDATE_EMAIL)){
      $this->email = $email;
    }
  }

}
```